**Important Note**
> We are currently restructuring the whole project. Please exspect a few changes in Namespaces and the GitLab URL!
> The project is shifting to a modular build where other applications addresses can be added with modules.

<div align="center">
  <a href="http://pwnadventure.com/">
    <img src="http://pwnadventure.com/img/logo.png" alt="Logo" height="250">
    <img src="https://gitlab.com/onsive.net/pwnadventure3/raw/master/.src/PwnAdventure3Client.ico" alt="Logo" height="250">
  </a>

  <h3 align="center">Pwn Adventure 3: Pwnie Island</h3>

  <p align="center">
    Hacks and Proxy Server
    <br />
    <a href="/issues/new">Report Bug</a>
    ·
    <a href="/issues/new">Request Feature</a>
  </p>
</div>

## Getting Started

This Repo provides you with all kinds of Hack Clients (also known as Trainers) and probably a few Proxy Server.

> ### Welcome to Pwnie Island!
> Pwn Adventure 3: Pwnie Island is a limited-release, first-person, true open-world MMORPG set on a beautiful island where anything could happen. That's because this game is intentionally vulnerable to all kinds of silly hacks! Flying, endless cash, and more are all one client change or network proxy away. Are you ready for the mayhem?!

[![][image1]][image1]

### Installing

Simply download the (mostly windows) applications via *comming soon...*.

**OR** build it with VisualStudio...

## Built With

* [VisualStudio19][vs19]

## Contributing

<s>Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.</s> *comming soon...*

## Authors

* **OnSive** - *Initial work and [onsive.net][onsive.net] leader* - [@OnSive][onsive]

See also the list of [contributors][contributors] who participated in this project.

## License

<a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/4.0/">Creative Commons Attribution-NonCommercial 4.0 International License</a> - see the [LICENSE.md](LICENSE) file for details

## Acknowledgments

* WIP
* [PwnAdventure3][PwmAdventure] official homepage *(that's the page where I ripped of the images)*

[image-hack]: https://gitlab.com/onsive.net/pwnadventure3/raw/master/.src/PwnAdventure3Client.ico
[image1]: http://pwnadventure.com/img/ss-pirates.jpg

[PwmAdventure]: http://pwnadventure.com/img/ss-pirates.jpg
[vs19]: https://docs.microsoft.com/en-us/visualstudio/windows/?view=vs-2019

[contributors]: https://gitlab.com/onsive.net/pwnadventure3/-/graphs/master

[onsive.net]: https://gitlab.com/onsive.net
[onsive]: https://gitlab.com/onsive
