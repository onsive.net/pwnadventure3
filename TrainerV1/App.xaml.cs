﻿using System.Windows;

namespace OnSive.Trainer.v1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            this.MainWindow = new wpfMainWindow();
            this.MainWindow.ShowDialog();

            System.Windows.Application.Current.Shutdown();
        }
    }
}
