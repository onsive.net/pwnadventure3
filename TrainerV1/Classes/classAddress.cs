﻿using System;
using System.Windows.Controls;
using OnSive.Trainer.v1.Classes.classEnums;

namespace OnSive.Trainer.v1.Classes
{
    public class classAddress
    {
        public string sName { get; private set; }

        public Int32 iAddress { get; private set; }

        public enValueType ValueType { get; private set; }

        public enDisplayType DisplayType { get; private set; }

        public Slider slider { get; private set; }

        public classAddress(string sName, Int32 iAddress, enValueType ValueType, enDisplayType DisplayType)
        {
            this.sName = sName;
            this.iAddress = iAddress;
            this.ValueType = ValueType;
            this.DisplayType = DisplayType;
        }

        public classAddress(string sName, Int32 iAddress, enValueType ValueType, int iMin, int iMax, int iChange)
        {
            this.sName = sName;
            this.iAddress = iAddress;
            this.ValueType = ValueType;
            this.DisplayType = enDisplayType.Slider;
            slider = new Slider
            {
                Minimum = iMin,
                Maximum = iMax,
                SmallChange = iChange,
                LargeChange = iChange
            };
        }
    }
}
