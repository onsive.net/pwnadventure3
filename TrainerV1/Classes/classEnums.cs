﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnSive.Trainer.v1.Classes.classEnums
{
    public enum enValueType
    {
        Double,
        Float,
        Int32,
        UInt32
    }

    public enum enDisplayType
    {
        ReadOnly,
        Text,
        Slider
    }
}
