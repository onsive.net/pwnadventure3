﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using OnSive.Trainer.v1.Classes.classEnums;

namespace OnSive.Trainer.v1.Classes
{
    public abstract class classModule : IDisposable
    {
        private bool bDisposed = false;

        public Process process { get; protected set; }

        private classMemory Memory { get; set; }

        public Dictionary<Guid, classAddress> Dictionary_Addresses { get; private set; }

        public classModule(Process process)
        {
            this.process = process;

            Dictionary_Addresses = new Dictionary<Guid, classAddress>();

            if (process != null)
            {
                Memory = new classMemory(process);
            }
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);

            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool bDisposing)
        {
            if (bDisposed)
            {
                return;
            }

            if (bDisposing)
            {
                // Dispose of Memory
                Memory.Dispose();
            }

            bDisposed = true;
        }

        /// <summary>
        /// Adds Address address as readonly
        /// </summary>
        public void Add_Address(string sName, Int32 iAddress, enValueType Type)
        {
            Dictionary_Addresses.Add(Guid.NewGuid(), new classAddress(sName, iAddress, Type, enDisplayType.ReadOnly));
        }

        /// <summary>
        /// Adds Address address with specific display type
        /// </summary>
        public void Add_Address(string sName, Int32 iAddress, enValueType ValueType, enDisplayType DisplayType)
        {
            Dictionary_Addresses.Add(Guid.NewGuid(), new classAddress(sName, iAddress, ValueType, DisplayType));
        }

        /// <summary>
        /// Adds Address address with slider
        /// </summary>
        public void Add_Address(string sName, Int32 iAddress, enValueType ValueType, int iMin, int iMax, int iChange)
        {
            Dictionary_Addresses.Add(Guid.NewGuid(), new classAddress(sName, iAddress, ValueType, iMin, iMax, iChange));
        }

        public string Read(Guid Guid_Address)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];

            switch (Address.ValueType)
            {
                case enValueType.Double:
                    return Read_Double(Guid_Address).ToString();
                case enValueType.Float:
                    return Read_Float(Guid_Address).ToString();
                case enValueType.Int32:
                    return Read_Int32(Guid_Address).ToString();
                case enValueType.UInt32:
                    return Read_UInt32(Guid_Address).ToString();
                default:
                    throw new InvalidCastException($"{Address.sName} is not of a valid type!");
            }
        }

        public double Read_Double(Guid Guid_Address)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];
            if (Address.ValueType != enValueType.Double)
            {
                throw new InvalidCastException($"{Address.sName} is not of type double!");
            }

            return Memory.Read_Double(Address.iAddress);
        }

        public float Read_Float(Guid Guid_Address)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];
            if (Address.ValueType != enValueType.Float)
            {
                throw new InvalidCastException($"{Address.sName} is not of type float!");
            }

            return Memory.Read_Float(Address.iAddress);
        }

        public Int32 Read_Int32(Guid Guid_Address)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];
            if (Address.ValueType != enValueType.Int32)
            {
                throw new InvalidCastException($"{Address.sName} is not of type Int32!");
            }

            return Memory.Read_Int32(Address.iAddress);
        }

        public UInt32 Read_UInt32(Guid Guid_Address)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];
            if (Address.ValueType != enValueType.UInt32)
            {
                throw new InvalidCastException($"{Address.sName} is not of type UInt32!");
            }

            return Memory.Read_UInt32(Address.iAddress);
        }

        public void Write_Double(Guid Guid_Address, double dValue)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];

            if (Address.DisplayType == enDisplayType.ReadOnly)
            {
                throw new InvalidOperationException($"{Address.sName} is set to readonly!");
            }

            if (Address.ValueType != enValueType.Double)
            {
                throw new InvalidCastException($"{Address.sName} is not of type double!");
            }

            Memory.Write_Double(Address.iAddress, dValue);
        }

        public void Write_Float(Guid Guid_Address, float fValue)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];

            if (Address.DisplayType == enDisplayType.ReadOnly)
            {
                throw new InvalidOperationException($"{Address.sName} is set to readonly!");
            }

            if (Address.ValueType != enValueType.Float)
            {
                throw new InvalidCastException($"{Address.sName} is not of type float!");
            }

            Memory.Write_Float(Address.iAddress, fValue);
        }

        public void Write_Int32(Guid Guid_Address, Int32 Int32_Value)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];

            if (Address.DisplayType == enDisplayType.ReadOnly)
            {
                throw new InvalidOperationException($"{Address.sName} is set to readonly!");
            }

            if (Address.ValueType != enValueType.Int32)
            {
                throw new InvalidCastException($"{Address.sName} is not of type Int32!");
            }

            Memory.Write_Int32(Address.iAddress, Int32_Value);
        }

        public void Write_UInt32(Guid Guid_Address, UInt32 UInt32_Value)
        {
            classAddress Address = Dictionary_Addresses[Guid_Address];

            if (Address.DisplayType == enDisplayType.ReadOnly)
            {
                throw new InvalidOperationException($"{Address.sName} is set to readonly!");
            }

            if (Address.ValueType != enValueType.UInt32)
            {
                throw new InvalidCastException($"{Address.sName} is not of type UInt32!");
            }

            Memory.Write_UInt32(Address.iAddress, UInt32_Value);
        }
    }
}
