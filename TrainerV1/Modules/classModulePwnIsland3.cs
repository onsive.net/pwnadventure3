﻿using System;
using System.Diagnostics;
using System.Linq;
using OnSive.Trainer.v1.Classes;
using OnSive.Trainer.v1.Classes.classEnums;

namespace OnSive.Trainer.v1.Modules
{
    class classModulePwnIsland3 : classModule
    {
        public classModulePwnIsland3() : base(Process.GetProcessesByName("PwnAdventure3-Win32-Shipping").FirstOrDefault())
        {
            base.Add_Address("Health", 0x405f9e10, enValueType.Int32, 0, 1000, 10);
        }
    }
}
