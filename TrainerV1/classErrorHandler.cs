﻿using System;
using System.Windows;

namespace OnSive.Trainer.v1
{
    public static class classErrorHandler
    {
        /// <summary>
        /// Error Handler Function
        /// </summary>
        /// <param name="ex">The Error</param>
        public static void On_Error(Exception ex)
        {
            // Prints the error in the console
            Console.WriteLine("#####");
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
            Console.WriteLine("#####");

            // Prints the error in a MessageBox
            MessageBox.Show(ex.Message + Environment.NewLine + Environment.NewLine + ex.StackTrace, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
