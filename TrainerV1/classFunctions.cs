﻿using OnSive.Trainer.v1.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace OnSive.Trainer.v1
{
    public static class classFunctions
    {
        internal static IEnumerable<Type> Get_TypesInNamespace(Assembly assembly, string nameSpace)
        {
            return assembly.GetTypes().Where(t => String.Equals(t.Namespace, nameSpace, StringComparison.Ordinal));
        }
    }
}
