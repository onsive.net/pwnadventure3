﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace OnSive.Trainer.v1
{
    /// <summary>
    /// Represents an access to a remote process memory
    /// </summary>
    public class classMemory : IDisposable
    {
        [DllImport("kernel32.dll")]
        private static extern IntPtr OpenProcess(ProcessAccessFlags dwDesiredAccess, [MarshalAs(UnmanagedType.Bool)] bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll")]
        public static extern bool ReadProcessMemory(int hProcess, int lpBaseAddress, byte[] lpBuffer, int dwSize, ref int lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, uint nSize, out int lpNumberOfBytesWritten);

        [DllImport("kernel32.dll")]
        public static extern Int32 CloseHandle(IntPtr hProcess);

        /// <summary>
        /// All process access flags
        /// </summary>
        [Flags]
        private enum ProcessAccessFlags : uint
        {
            All = 0x001F0FFF,
            Terminate = 0x00000001,
            CreateThread = 0x00000002,
            VMOperation = 0x00000008,
            VMRead = 0x00000010,
            VMWrite = 0x00000020,
            DupHandle = 0x00000040,
            SetInformation = 0x00000200,
            QueryInformation = 0x00000400,
            Synchronize = 0x00100000
        }

        private IntPtr IntPtr_ProcessHandle;
        private bool bIsDisposed;

        private const string sOffsetPattern = "(\\+|\\-){0,1}(0x){0,1}[a-fA-F0-9]{1,}";

        /// <summary>
        /// Gets the process to which this memory is attached to
        /// </summary>
        public Process Process { get; private set; }

        /// <summary>
        /// Initializes a new instance of the Memory
        /// </summary>
        /// <param name="Process_Tmp">Remote process</param>
        public classMemory(Process Process_Tmp)
        {
            // If there was no process passed
            if (Process_Tmp == null)
            {
                // Throw new Exception
                throw new ArgumentNullException("process");
            }
            // Sets the process proterty
            this.Process = Process_Tmp;

            // Initializes the IntPtr with the opened process and r/w flags
            IntPtr_ProcessHandle = OpenProcess(
                ProcessAccessFlags.VMRead | ProcessAccessFlags.VMWrite |
                ProcessAccessFlags.VMOperation, true, Process_Tmp.Id);

            // If the process can't be opened
            if (IntPtr_ProcessHandle == IntPtr.Zero)
            {
                // Throw new exception
                throw new InvalidOperationException("Could not open the process");
            }
        }

        /// <summary>
        /// Disposes the object
        /// </summary>
        ~classMemory()
        {
            Dispose(false);
        }

        /// <summary>
        /// Disposes the object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes the object
        /// </summary>
        private void Dispose(bool disposing)
        {
            // If the object is already disposed
            if (bIsDisposed)
            {
                // Return
                return;
            }
            // Close the IntPtr
            CloseHandle(IntPtr_ProcessHandle);

            // Removes the process
            Process = null;

            // Removes the IntPtr
            IntPtr_ProcessHandle = IntPtr.Zero;

            // Sets the object as disposed
            bIsDisposed = true;
        }

        /// <summary>
        /// Finds module with the given name
        /// </summary>
        /// <param name="sName">Module name</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">If there was no name passed</exception>
        protected ProcessModule Get_Modul(string sName)
        {
            // If no modul name was passed
            if (string.IsNullOrEmpty(sName))
            {
                // Throws new exception
                throw new ArgumentNullException("name");
            }
            // Foreach module in the process
            foreach (ProcessModule ProcessModule_Tmp in Process.Modules)
            {
                // If the module name is the searched name
                if (ProcessModule_Tmp.ModuleName.ToLower() == sName.ToLower())
                {
                    // Returns the ProcessModul
                    return ProcessModule_Tmp;
                }
            }
            // Returns null as there was no module found
            return null;
        }

        /// <summary>
        /// Gets module based address
        /// </summary>
        /// <param name="sModulName">Module name</param>
        /// <param name="IntrPtr_BaseAddress">Base address</param>
        /// <param name="IOffsets">Collection of offsets</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">If there was no modul name passed</exception>
        public IntPtr Get_Address(string sModulName, IntPtr IntrPtr_BaseAddress, int[] IOffsets)
        {
            // If no modul name was passed
            if (string.IsNullOrEmpty(sModulName))
            {
                // Throws new exception
                throw new ArgumentNullException("moduleName");
            }
            // Trys to get the modul with the modul name
            ProcessModule ProcessModule_Tmp = Get_Modul(sModulName);

            // If there was no module found
            if (ProcessModule_Tmp == null)
            {
                // Returns a empty IntPtr
                return IntPtr.Zero;
            }
            else
            {
                // Adds the base address and the pointer
                long lAddress = ProcessModule_Tmp.BaseAddress.ToInt64() + IntrPtr_BaseAddress.ToInt64();

                // Returns the new builded address
                return Get_Address((IntPtr) lAddress, IOffsets);
            }
        }

        /// <summary>
        /// Gets address
        /// </summary>
        /// <param name="IntPtr_BaseAddress">Base address</param>
        /// <param name="IOffsets">Collection of offsets</param>
        /// <returns></returns>
        public IntPtr Get_Address(IntPtr IntPtr_BaseAddress, int[] IOffsets)
        {
            // If there was no base address passed
            if (IntPtr_BaseAddress == IntPtr.Zero)
            {
                // Throws new exception
                throw new ArgumentException("Invalid base address");
            }
            // Converts the IntPtr to long
            long lAddress = IntPtr_BaseAddress.ToInt64();

            // If there was a offset passed
            if (IOffsets != null && IOffsets.Length > 0)
            {
                // Initializes a new 4 Byte buffer
                byte[] Byte_Buffer = new byte[4];

                // Foreach offset
                foreach (int iOffset in IOffsets)
                {
                    // Adds the offset to the address
                    lAddress = Read_Int32((IntPtr) lAddress) + iOffset;
                }
            }
            // Returns the final address
            return (IntPtr) lAddress;
        }

        /// <summary>
        /// Gets address pointer
        /// </summary>
        /// <param name="sAddress">Address</param>
        /// <returns></returns>
        public IntPtr Get_Address(string sAddress)
        {
            // If no address was passed
            if (string.IsNullOrEmpty(sAddress))
            {
                // Throws new exception
                throw new ArgumentNullException("address");
            }
            // Initialize the module name
            string sModuleName = null;

            // Gets the index on the first "
            int iIndex = sAddress.IndexOf('"');

            // If there was no index found
            if (iIndex != -1)
            {
                // Gets the index of the second "
                int iIndexEnd = sAddress.IndexOf('"', iIndex + 1);

                // If there was no second index found
                if (iIndexEnd == -1)
                {
                    // Throws new exception
                    throw new ArgumentException("Invalid module name. Could not find matching \"");
                }
                // Gets the module name from the address with the start and end index
                sModuleName = sAddress.Substring(iIndex + 1, iIndexEnd - 1);

                // Removes the module name from the address
                sAddress = sAddress.Substring(iIndexEnd + 1);
            }
            // Get the base address and address offsets
            int[] IOffsets_WithBaseAddress = Get_Address_Offsets(sAddress);

            // Initialize the final offset array
            int[] IOffsets = null;

            // Set the IntPtr of the base address if there was a offset list found
            IntPtr IntPtr_BaseAddress = IOffsets_WithBaseAddress != null && IOffsets_WithBaseAddress.Length > 0 ? (IntPtr) IOffsets_WithBaseAddress[0] : IntPtr.Zero;

            // If there was a offset list found
            if (IOffsets_WithBaseAddress != null && IOffsets_WithBaseAddress.Length > 1)
            {
                // Initialize the final offset array without the base address
                IOffsets = new int[IOffsets_WithBaseAddress.Length - 1];

                // Foreach offset
                for (int i = 0; i < IOffsets_WithBaseAddress.Length - 1; i++)
                {
                    // Set all offsets in the final offset array (without the base address)
                    IOffsets[i] = IOffsets_WithBaseAddress[i + 1];
                }
            }
            // If there was no module name found
            if (sModuleName != null)
            {
                // Returns the address
                return Get_Address(sModuleName, IntPtr_BaseAddress, IOffsets);
            }
            else
            {
                // Returns the address
                return Get_Address(IntPtr_BaseAddress, IOffsets);
            }
        }

        /// <summary>
        /// Gets address offsets
        /// </summary>
        /// <param name="sAddress">Address</param>
        /// <returns></returns>
        protected static int[] Get_Address_Offsets(string sAddress)
        {
            // If there was no address passed
            if (string.IsNullOrEmpty(sAddress))
            {
                // Returns empty array
                return new int[0];
            }
            else
            {
                // Searches for offsets with the pattern
                MatchCollection MatchCollection_Offsets = Regex.Matches(sAddress, sOffsetPattern);

                // Initializes a new offset array with the found ones
                int[] IOffsets = new int[MatchCollection_Offsets.Count];

                // The hex value
                string value;

                // The math operator (+/-)
                char cOperator;

                // Foreach offset
                for (int i = 0; i < MatchCollection_Offsets.Count; i++)
                {
                    // Set the math operator
                    cOperator = MatchCollection_Offsets[i].Value[0];

                    // If the math operator is a + or -
                    if (cOperator == '+' || cOperator == '-')
                    {
                        // Writes the hex value without the operator
                        value = MatchCollection_Offsets[i].Value.Substring(1);
                    }
                    else
                    {
                        // Writes the hex value
                        value = MatchCollection_Offsets[i].Value;
                    }
                    // Set the value as int to the index position
                    IOffsets[i] = Convert.ToInt32(value, 16);

                    // If the operator is a -
                    if (cOperator == '-')
                    {
                        // Sets the value to the negative
                        IOffsets[i] = -IOffsets[i];
                    }
                }
                // Returns the offset array
                return IOffsets;
            }
        }

        /// <summary>
        /// Reads memory at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <param name="BBuffer">Buffer</param>
        /// <param name="iSize">Size in bytes</param>
        public void Read_Memory(IntPtr IntPtr_Address, byte[] BBuffer, int iSize)
        {
            // If the object is already disposed
            if (bIsDisposed)
            {
                // Throws new exception
                throw new ObjectDisposedException("Memory");
            }
            // If there was no buffer passed
            if (BBuffer == null)
            {
                // Throws new exception
                throw new ArgumentNullException("buffer");
            }
            // If there was a zero or negative size passed
            if (iSize <= 0)
            {
                // Throws new exception
                throw new ArgumentException("Size must be greater than zero");
            }
            // If there was no address passed
            if (IntPtr_Address == IntPtr.Zero)
            {
                // Throws new exception
                throw new ArgumentException("Invalid address");
            }
            // Validation value
            int iRead = 0;

            // If the read process failed or the validation value is not the size
            if (!ReadProcessMemory(IntPtr_ProcessHandle.ToInt32(), IntPtr_Address.ToInt32(), BBuffer, (int) iSize, ref iRead) || iRead != iSize)
            {
                // Throws new exception
                throw new AccessViolationException();
            }
        }

        /// <summary>
        /// Writes memory at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <param name="BBuffer">Buffer</param>
        /// <param name="iSsize">Size in bytes</param>
        public void Write_Memory(IntPtr IntPtr_Address, byte[] BBuffer, int iSsize)
        {
            // If the object is already disposed
            if (bIsDisposed)
            {
                // Throws new exception
                throw new ObjectDisposedException("Memory");
            }
            // If there was no buffer passed
            if (BBuffer == null)
            {
                // Throws new exception
                throw new ArgumentNullException("buffer");
            }
            // If there was a zero or negative size passed
            if (iSsize <= 0)
            {
                // Throws new exception
                throw new ArgumentException("Size must be greater than zero");
            }
            // If there was no address passed
            if (IntPtr_Address == IntPtr.Zero)
            {
                // Throws new exception
                throw new ArgumentException("Invalid address");
            }
            // Validation value
            int iWrite = 0;

            // If the write process failed or the validation value is not the size
            if (!WriteProcessMemory(IntPtr_ProcessHandle, IntPtr_Address, BBuffer, (uint) iSsize, out iWrite) || iWrite != iSsize)
            {
                // Throws new exception
                throw new AccessViolationException();
            }
        }

        /// <summary>
        /// Reads 32 bit signed integer at the address
        /// </summary>
        /// <param name="Address">Memory address</param>
        /// <returns></returns>
        public int Read_Int32(int Address)
        {
            // Reads 32 bit signed integer at the address
            return Read_Int32((IntPtr) Address);
        }

        /// <summary>
        /// Reads 32 bit signed integer at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <returns></returns>
        public int Read_Int32(IntPtr IntPtr_Address)
        {
            // Initialize a new 4 Byte buffer
            byte[] BBuffer = new byte[4];

            // Reads the integer at the address
            Read_Memory(IntPtr_Address, BBuffer, 4);

            // Returns the integer value
            return BitConverter.ToInt32(BBuffer, 0);
        }

        /// <summary>
        /// Reads 32 bit unsigned integer at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <returns></returns>
        public uint Read_UInt32(int Address)
        {
            // Reads 32 bit unsigned integer at the address
            return Read_UInt32((IntPtr) Address);
        }

        /// <summary>
        /// Reads 32 bit unsigned integer at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <returns></returns>
        public uint Read_UInt32(IntPtr IntPtr_Address)
        {
            // Initialize a new 4 Byte buffer
            byte[] BBuffer = new byte[4];

            // Reads the uinteger at the address
            Read_Memory(IntPtr_Address, BBuffer, 4);

            // Returns the uinteger value
            return BitConverter.ToUInt32(BBuffer, 0);
        }

        /// <summary>
        /// Reads single precision value at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <returns></returns>
        public float Read_Float(int Address)
        {
            // Reads single precision value at the address
            return Read_Float((IntPtr) Address);
        }

        /// <summary>
        /// Reads single precision value at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <returns></returns>
        public float Read_Float(IntPtr IntPtr_Address)
        {
            // Initialize a new 4 Byte buffer
            byte[] BBuffer = new byte[4];

            // Reads the float at the address
            Read_Memory(IntPtr_Address, BBuffer, 4);

            // Returns the float value
            return BitConverter.ToSingle(BBuffer, 0);
        }

        /// <summary>
        /// Reads double precision value at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <returns></returns>
        public double Read_Double(int Address)
        {
            // Reads double precision value at the address
            return Read_Double((IntPtr) Address);
        }

        /// <summary>
        /// Reads double precision value at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <returns></returns>
        public double Read_Double(IntPtr IntPtr_Address)
        {
            // Initialize a new 4 Byte buffer
            byte[] BBuffer = new byte[8];

            // Reads the double at the address
            Read_Memory(IntPtr_Address, BBuffer, 8);

            // Returns the double value
            return BitConverter.ToDouble(BBuffer, 0);
        }

        /// <summary>
        /// Writes 32 bit unsigned integer at the address
        /// </summary>
        /// <param name="Address">Memory address</param>
        /// <param name="uint_Value">Value</param>
        /// <returns></returns>
        public void Write_UInt32(int Address, uint uint_Value)
        {
            // Writes 32 bit unsigned integer at the address
            Write_UInt32((IntPtr) Address, uint_Value);
        }

        /// <summary>
        /// Writes 32 bit unsigned integer at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <param name="uint_Value">Value</param>
        /// <returns></returns>
        public void Write_UInt32(IntPtr IntPtr_Address, uint uint_Value)
        {
            // Writes the value to the buffer
            byte[] BBuffer = BitConverter.GetBytes(uint_Value);

            // Writes the buffer to the address in the memory
            Write_Memory(IntPtr_Address, BBuffer, 4);
        }

        /// <summary>
        /// Writes 32 bit signed integer at the address
        /// </summary>
        /// <param name="Address">Memory address</param>
        /// <param name="iValue">Value</param>
        /// <returns></returns>
        public void Write_Int32(int Address, int iValue)
        {
            // Writes 32 bit signed integer at the address
            Write_Int32((IntPtr) Address, iValue);
        }

        /// <summary>
        /// Writes 32 bit signed integer at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <param name="iValue">Value</param>
        /// <returns></returns>
        public void Write_Int32(IntPtr IntPtr_Address, int iValue)
        {
            // Writes the value to the buffer
            byte[] BBuffer = BitConverter.GetBytes(iValue);

            // Writes the buffer to the address in the memory
            Write_Memory(IntPtr_Address, BBuffer, 4);
        }

        /// <summary>
        /// Writes single precision value at the address
        /// </summary>
        /// <param name="Address">Memory address</param>
        /// <param name="fValue">Value</param>
        /// <returns></returns>
        public void Write_Float(int Address, float fValue)
        {
            // Writes single precision value at the address
            Write_Float((IntPtr) Address, fValue);
        }

        /// <summary>
        /// Writes single precision value at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <param name="fValue">Value</param>
        /// <returns></returns>
        public void Write_Float(IntPtr IntPtr_Address, float fValue)
        {
            // Writes the value to the buffer
            byte[] BBuffer = BitConverter.GetBytes(fValue);

            // Writes the buffer to the address in the memory
            Write_Memory(IntPtr_Address, BBuffer, 4);
        }

        /// <summary>
        /// Writes double precision value at the address
        /// </summary>
        /// <param name="IntPtr_Address">Memory address</param>
        /// <param name="dValue">Value</param>
        /// <returns></returns>
        public void Write_Double(int Address, double dValue)
        {
            // Writes double precision value at the address
            Write_Double((IntPtr) Address, dValue);
        }

        /// <summary>
        /// Writes double precision value at the address
        /// </summary>
        /// <param name="Address">Memory address</param>
        /// <param name="dValue">Value</param>
        /// <returns></returns>
        public void Write_Double(IntPtr IntPtr_Address, double dValue)
        {
            // Writes the value to the buffer
            byte[] BBuffer = BitConverter.GetBytes(dValue);

            // Writes the buffer to the address in the memory
            Write_Memory(IntPtr_Address, BBuffer, 8);
        }
    }
}