﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using OnSive.Trainer.v1.Classes;
using OnSive.Trainer.v1.Classes.classEnums;

namespace OnSive.Trainer.v1
{
    /// <summary>
    /// Interaction logic for wpfMainWindow.xaml
    /// </summary>
    public partial class wpfMainWindow : Window
    {
        public classModule Module { get; set; }

        public wpfMainWindow()
        {
            try
            {
                InitializeComponent();

                this.Loaded += this.WpfMainWindow_Loaded;
            }
            catch (Exception ex)
            {
                classErrorHandler.On_Error(ex);
            }
        }

        private void WpfMainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                wpfModuleSelection wpfModuleSelection = new wpfModuleSelection();
                wpfModuleSelection.Owner = this;
                wpfModuleSelection.ShowDialog();

                if (Module == null)
                {
                    this.Close();
                }

                this.Title = $"Trainer v1 | [{Module.process.Id}] {Module.process.ProcessName}";

                foreach (KeyValuePair<Guid, classAddress> KeyValuePair_Address in Module.Dictionary_Addresses)
                {
                    try
                    {
                        switch (KeyValuePair_Address.Value.DisplayType)
                        {
                            case enDisplayType.Text:

                                DockPanel dp1 = new DockPanel();
                                dp1.Tag = KeyValuePair_Address.Key;

                                dp1.Children.Add(new Label { Content = KeyValuePair_Address.Value.sName, Width = 150 });
                                dp1.Children.Add(new TextBox { Text = Module.Read(KeyValuePair_Address.Key) });

                                StackPanel_Addresses.Children.Add(dp1);
                                break;

                            case enDisplayType.Slider:

                                DockPanel dp2 = new DockPanel();
                                dp2.Tag = KeyValuePair_Address.Key;

                                dp2.Children.Add(new Label { Content = KeyValuePair_Address.Value.sName, Width = 150 });
                                dp2.Children.Add(KeyValuePair_Address.Value.slider);

                                StackPanel_Addresses.Children.Add(dp2);
                                break;

                            case enDisplayType.ReadOnly:

                                DockPanel dp3 = new DockPanel();
                                dp3.Tag = KeyValuePair_Address.Key;

                                dp3.Children.Add(new Label { Content = KeyValuePair_Address.Value.sName, Width = 150 });
                                dp3.Children.Add(new Label { Content = Module.Read(KeyValuePair_Address.Key) });

                                StackPanel_Addresses.Children.Add(dp3);
                                break;

                            default:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show($"{ex.Message}\n\n{ex.StackTrace}",
                                        $"Trainer v1 | [{Module.process.Id}] {Module.process.ProcessName}",
                                        MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                classErrorHandler.On_Error(ex);
            }
        }
    }
}
