﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using OnSive.Trainer.v1.Classes;

namespace OnSive.Trainer.v1
{
    /// <summary>
    /// Interaction logic for wpfModuleSelection.xaml
    /// </summary>
    public partial class wpfModuleSelection : Window
    {
        public wpfModuleSelection()
        {
            try
            {
                InitializeComponent();

                IEnumerable<Type> Enumerable_Moduls = classFunctions.Get_TypesInNamespace(Assembly.GetExecutingAssembly(), "OnSive.Trainer.v1.Modules");

                foreach (Type item in Enumerable_Moduls)
                {
                    classModule Module = (classModule)Activator.CreateInstance(item);
                    if (Module.process == null)
                    {
                        continue;
                    }

                    ListBoxItem lbi = new ListBoxItem();
                    lbi.Content = item.Name;
                    lbi.Tag = Module;
                    ListBox_ModuleSelection.Items.Add(lbi);
                }
            }
            catch (Exception ex)
            {
                classErrorHandler.On_Error(ex);
            }
        }

        private void ListBox_ModuleSelection_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                ((wpfMainWindow)this.Owner).Module = ((classModule)((ListBoxItem)ListBox_ModuleSelection.SelectedItem).Tag);
            }
            catch (Exception ex)
            {
                classErrorHandler.On_Error(ex);
            }
        }
    }
}
